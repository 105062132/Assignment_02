var game = new Phaser.Game(400, 400, Phaser.AUTO, '',
    { preload: preload, create: create, update: update });

var player;
var keyboard;

var platforms = [];
var shield;
var emitter;

var leftWall;
var rightWall;
var ceiling;
var pause_label;

var text1;
var text2;
var text3;
var text4;
var text5;
var text6;
var score_temp;

var music;
var music_die;
var distance = 0;
var status = 'menu';
var count = 0;
var count2 = 0;
//firebase


//
function preload () {

    game.load.spritesheet('player', 'assets/player.png', 32, 32);
    game.load.image('pixel', 'assets/pixel.png');
    game.load.image('bg', 'assets/bg.png');
    game.load.image('wall', 'assets/wall.png');
    game.load.image('ceiling', 'assets/ceiling.png');
    game.load.image('normal', 'assets/normal.png');
    game.load.image('nails', 'assets/nails.png');
    game.load.image('shield','assets/shield.png');
    game.load.spritesheet('conveyorRight', 'assets/conveyor_right.png', 96, 16);
    game.load.spritesheet('conveyorLeft', 'assets/conveyor_left.png', 96, 16);
    game.load.spritesheet('trampoline', 'assets/trampoline.png', 96, 22);
    game.load.spritesheet('fake', 'assets/fake.png', 96, 36);
    game.load.audio('bgm', 'assets/hit.wav');
    game.load.audio('die', 'assets/die.mp3');
}

function create () {

    keyboard = game.input.keyboard.addKeys({
        'enter': Phaser.Keyboard.ENTER,
        'up': Phaser.Keyboard.UP,
        'down': Phaser.Keyboard.DOWN,
        'left': Phaser.Keyboard.LEFT,
        'right': Phaser.Keyboard.RIGHT,
        'w': Phaser.Keyboard.W,
        'a': Phaser.Keyboard.A,
        's': Phaser.Keyboard.S,
        'd': Phaser.Keyboard.D,
        'p':Phaser.Keyboard.P
    });
    music = game.add.audio('bgm')
    music_die = game.add.audio('die');
    
    game.add.image(0,0,'bg');
    createBounders();
    //createPlayer();
    createTextsBoard();
    
    pause_label = game.add.text(game.width - 100, 460, 'Pause', { font: '24px Arial', fill: '#fff' });
    pause_label.inputEnabled = true;
    pause_label.events.onInputUp.add(function () {
        this.text3.visible = true;
        game.paused = true;
    })

    game.input.onDown.add(this.unpause, self);
}

function update () {

    // bad
    if((status == 'gameOver' || status == 'menu')&& keyboard.enter.isDown) restart();
    /*if(status == 'pause') {
        updatePlayer();
    }*/
    if (status == 'running'){
       
        this.physics.arcade.collide(player, platforms, effect);
        if (shield != null)
        {
            this.physics.arcade.overlap(player, shield, unbeat);
        }
        if (shield != null)
            updateShields();
        this.physics.arcade.collide(player, [leftWall, rightWall]);
        if(game.time.now < player.unbeatableTime )
        {
            count2 +=1;
            if (count2%30 == 0)
                game.camera.flash(0x98fb98, 100);
        }
        checkTouchCeiling(player);
        checkGameOver();

        updatePlayer();
        updatePlatforms();
        
        updateTextsBoard();

        
        createPlatforms();
        if (keyboard.p.isDown){
            console.log("p is down");
            if(game.paused == false)
                game.paused = true;
            else if (game.paused == true)
                game.paused = false;
            //status = 'pause';
            //updatePlayer();
            //return;
        }
    }
    
}

function createBounders () {
    leftWall = game.add.sprite(0, 0, 'wall');
    game.physics.arcade.enable(leftWall);
    leftWall.body.immovable = true;

    rightWall = game.add.sprite(383, 0, 'wall');
    game.physics.arcade.enable(rightWall);
    rightWall.body.immovable = true;

    ceiling = game.add.image(0, 0, 'ceiling');
}

var lastTime = 0;
function createPlatforms () {
    if(game.time.now > lastTime + 600) {
        lastTime = game.time.now;
        createOnePlatform();
        //createOneshield();
        distance += 1;
        count+=1;
        if (count == 10)
        {
            count = 0;
            createOneshield();
        }
    }
}

function createOneshield(){
    
    var x = Math.random()*(400 - 96 - 40) + 20;
    var y = 400;
    shield = game.add.sprite(x,y,'shield');
    shield.anchor.setTo(0.5,0);
    game.physics.arcade.enable(shield);
    //shields.push(shield);
}
function createOnePlatform () {

    var platform;
    var x = Math.random()*(400 - 96 - 40) + 20;
    var y = 400;
    var rand = Math.random() * 100;

    if(rand < 40) {
        platform = game.add.sprite(x, y, 'normal');
    } else if (rand < 50) {
        platform = game.add.sprite(x, y, 'nails');
        game.physics.arcade.enable(platform);
        platform.body.setSize(96, 15, 0, 15);
    } else if (rand < 65) {
        platform = game.add.sprite(x, y, 'conveyorLeft');
        platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
        platform.play('scroll');
    } else if (rand < 80) {
        platform = game.add.sprite(x, y, 'conveyorRight');
        platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
        platform.play('scroll');
    } else if (rand < 90) {
        platform = game.add.sprite(x, y, 'trampoline');
        platform.animations.add('jump', [4, 5, 4, 3, 2, 1, 0, 1, 2, 3], 120);
        platform.frame = 3;
    } else {
        platform = game.add.sprite(x, y, 'fake');
        platform.animations.add('turn', [0, 1, 2, 3, 4, 5, 0], 14);
    }

    game.physics.arcade.enable(platform);
    platform.body.immovable = true;
    platforms.push(platform);

    platform.body.checkCollision.down = false;
    platform.body.checkCollision.left = false;
    platform.body.checkCollision.right = false;
}

function createPlayer() {
    player = game.add.sprite(200, 50, 'player');
    player.direction = 10;
    game.physics.arcade.enable(player);
    player.body.gravity.y = 900;
    player.animations.add('left', [0, 1, 2, 3], 8);
    player.animations.add('right', [9, 10, 11, 12], 8);
    player.animations.add('flyleft', [18, 19, 20, 21], 12);
    player.animations.add('flyright', [27, 28, 29, 30], 12);
    player.animations.add('fly', [36, 37, 38, 39], 12);
    player.life = 10;
    player.unbeatableTime = 0;
    player.touchOn = undefined;
}

function createTextsBoard () {
    var style = {fill: '#ff0000', fontSize: '20px'}
    text1 = game.add.text(17, 10, '', {fill: '#f0f8ff', fontSize: '20px'});
    text2 = game.add.text(347, 10, '', {fill: '#f0f8ff', fontSize: '20px'});
    text3 = game.add.text(110, 320, '按Enter鍵 開始\n方向左右鍵控制角色', {fill: '#f5f5f5', fontSize: '22px'});
    text4 = game.add.text(140, 100, '小朋友下樓梯', {fill: '#ffc0cb', fontSize: '22px'});
    text5 = game.add.text(140, 50, 'Game Over', {fill: '#ffc0cb', fontSize: '24px'});
    text6 = game.add.text(140,90,'',{fill: '#e0ffff', fontSize: '22px'});
    text5.visible = false;
    text6.visible = false;
}

function updatePlayer () {
    if(keyboard.left.isDown) {
        player.body.velocity.x = -250;
    } else if(keyboard.right.isDown) {
        player.body.velocity.x = 250;
    } else {
        player.body.velocity.x = 0;
    }
    setPlayerAnimate(player);
}

function setPlayerAnimate(player) {
    var x = player.body.velocity.x;
    var y = player.body.velocity.y;

    if (x < 0 && y > 0) {
        player.animations.play('flyleft');
    }
    if (x > 0 && y > 0) {
        player.animations.play('flyright');
    }
    if (x < 0 && y == 0) {
        player.animations.play('left');
    }
    if (x > 0 && y == 0) {
        player.animations.play('right');
    }
    if (x == 0 && y != 0) {
        player.animations.play('fly');
    }
    if (x == 0 && y == 0) {
      player.frame = 8;
    }
}
function updateShields () {
        if(shield.body != null) {
            shield.body.position.y -= 2;
            if(shield.body.position.y <= -20) {
                shield.destroy();
            }
        }
        
    
}
function updatePlatforms () {
    for(var i=0; i<platforms.length; i++) {
        var platform = platforms[i];
        platform.body.position.y -= 2;
        if(platform.body.position.y <= -20) {
            platform.destroy();
            platforms.splice(i, 1);
        }
    }
}

function updateTextsBoard () {
    text1.setText('life:' + player.life);
    text2.setText('B' + distance);
}
function unbeat(){
    if(game.time.now > player.unbeatableTime) {
        //emitter
        emitter = game.add.emitter(shield.body.position.x, shield.body.position.y, 15);
        emitter.makeParticles('pixel');
        emitter.setYSpeed(-150, 150);
        emitter.setXSpeed(-150, 150);
        emitter.setScale(2, 0, 2, 0, 800);
        emitter.gravity = 0;
        emitter.start(true, 800, null, 15);
        //
        shield.kill();
        player.unbeatableTime = game.time.now + 4000;
        //game.camera.flash(0x98fb98, 40000);
        //player.life += 10;
        distance +=10;
    }
}
function effect(player, platform) {
    
    if(platform.key == 'conveyorRight') {
        conveyorRightEffect(player, platform);
    }
    if(platform.key == 'conveyorLeft') {
        conveyorLeftEffect(player, platform);
    }
    if(platform.key == 'trampoline') {
        trampolineEffect(player, platform);
    }
    if(platform.key == 'nails') {
        nailsEffect(player, platform);
    }
    if(platform.key == 'normal') {
        basicEffect(player, platform);
    }
    if(platform.key == 'fake') {
        fakeEffect(player, platform);
    }
}

function conveyorRightEffect(player, platform) {
    player.body.x += 2;
    if (player.touchOn !== platform) {
        player.touchOn = platform;
        if(player.life < 10) {
            player.life += 1;
        }
        music.play();
    }
}

function conveyorLeftEffect(player, platform) {
    player.body.x -= 2;
    if (player.touchOn !== platform) {
        player.touchOn = platform;
        if(player.life < 10) {
            player.life += 1;
        }
        music.play();
    }
}

function trampolineEffect(player, platform) {
    music.play();
    platform.animations.play('jump');
    player.body.velocity.y = -350;
    if(player.life < 10) {
        player.life += 1;
    }
    
}

function nailsEffect(player, platform) {
    if (player.touchOn !== platform) {
        if(game.time.now > player.unbeatableTime){
            player.life -= 3;
            
            game.camera.flash(0xff0000, 100);
        }
        player.touchOn = platform;
        music.play();
    }
}

function basicEffect(player, platform) {
    if (player.touchOn !== platform) {
        if(player.life < 10) {
            player.life += 1;
        }
        player.touchOn = platform;
        music.play();
    }
}

function fakeEffect(player, platform) {
    if(player.touchOn !== platform) {
        //if(game.time.now > player.unbeatableTime){
            platform.animations.play('turn');
            music.play();
            setTimeout(function() {
                platform.body.checkCollision.up = false;
            }, 100);
            player.touchOn = platform;
        //}
    }
}

function checkTouchCeiling(player) {
    if(player.body.y < 0) {
        if(player.body.velocity.y < 0) {
            player.body.velocity.y = 0;
        }
        if(game.time.now > player.unbeatableTime) {
            player.life -= 3;
            game.camera.flash(0xff0000, 100);
            player.unbeatableTime = game.time.now + 2000;
        }
    }
}

function checkGameOver () {
    if(player.life <= 0 || player.body.y > 500) {
        music_die.play();
        //setTimeout(gameOver, 800);
        
        gameOver();
    }
}

function gameOver () {
    
    //firebase
    var postsRef = firebase.database().ref('com_list');
    var name_player = prompt("Please enter your name", "name");
    if (name_player != null)
    {
        postsRef.push({
            name: name_player,
            data: distance,
            order:-distance
        });
    }
    var database = firebase.database().ref('com_list').orderByChild('order').limitToFirst(5);
    database.once('value', function(snapshot){
        var scoreText = 'LeaderBoard\n';
        snapshot.forEach(function(childSnapshot){
            scoreText += childSnapshot.val().name + '  ' + childSnapshot.val().data + '\n';
        });
        text6.setText(scoreText);
    });
    text6.visible = true;
    //
    text3.visible = true;
    text5.visible = true;
    platforms.forEach(function(s) {s.destroy()});
    platforms = [];
    //shields.forEach(function(s){s.destroy()});
    //shields = [];
    if (shield != null)
        shield.destroy();
    status = 'gameOver';
}

function restart () {
    text3.visible = false;
    text4.visible = false;
    text5.visible = false;
    text6.visible = false;
    distance = 0;
    count = 0;
    count2 = 0;
    createPlayer();
    status = 'running';
}